package com.realwear.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class BluetoothService(bluetoothCallBackListener: BluetoothCallBackListener) :
    BluetoothServiceContract {

    private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val btCallBackListener: BluetoothCallBackListener
    private var secureAcceptThread: AcceptThread? = null
    private var insecureAcceptThread: AcceptThread? = null
    private var connectThread: ConnectThread? = null
    private var connectedThread: ConnectedThread? = null
    private var bluetoothState: Int
    private var newBluetoothState: Int

    /**
     * Update UI title according to the current state of the chat connection
     */
    @Synchronized
    private fun updateUserInterfaceTitle() {
        bluetoothState = getBluetoothState()
        newBluetoothState = bluetoothState
        // Give the new state to the Handler so the UI Activity can update
        btCallBackListener.onStateChanged(newBluetoothState)
    }

    /**
     * Return the current connection state.
     */
    @Synchronized
    override fun getBluetoothState(): Int {
        return bluetoothState
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    @Synchronized
    override fun start() {
        // Cancel any thread attempting to make a connection
        if (connectThread != null) {
            connectThread!!.cancel()
            connectThread = null
        }

        // Cancel any thread currently running a connection
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        // Start the thread to listen on a BluetoothServerSocket
        if (secureAcceptThread == null) {
            secureAcceptThread = AcceptThread()
            secureAcceptThread!!.start()
        }
        if (insecureAcceptThread == null) {
            insecureAcceptThread = AcceptThread()
            insecureAcceptThread!!.start()
        }
        // Update UI title
        updateUserInterfaceTitle()
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    @Synchronized
    override fun connect(device: BluetoothDevice, secure: Boolean) {
        // Cancel any thread attempting to make a connection
        if (bluetoothState == STATE_CONNECTING) {
            if (connectThread != null) {
                connectThread!!.cancel()
                connectThread = null
            }
        }

        // Cancel any thread currently running a connection
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        // Start the thread to connect with the given device
        connectThread = ConnectThread(device, secure)
        connectThread!!.start()
        // Update UI
        updateUserInterfaceTitle()
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    @Synchronized
    private fun connected(socket: BluetoothSocket?, device: BluetoothDevice) {
        // Cancel the thread that completed the connection
        if (connectThread != null) {
            connectThread!!.cancel()
            connectThread = null
        }

        // Cancel any thread currently running a connection
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }

        // Cancel the accept thread because we only want to connect to one device
        if (secureAcceptThread != null) {
            secureAcceptThread!!.cancel()
            secureAcceptThread = null
        }
        if (insecureAcceptThread != null) {
            insecureAcceptThread!!.cancel()
            insecureAcceptThread = null
        }

        // Start the thread to manage the connection and perform transmissions
        connectedThread = ConnectedThread(socket)
        connectedThread!!.start()
        btCallBackListener.onDeviceConnected(device.name, device.address)
        // Update UI title
        updateUserInterfaceTitle()
    }

    /**
     * Stop all threads
     */
    @Synchronized
    override fun stop() {
        if (connectThread != null) {
            connectThread!!.cancel()
            connectThread = null
        }
        if (connectedThread != null) {
            connectedThread!!.cancel()
            connectedThread = null
        }
        if (secureAcceptThread != null) {
            secureAcceptThread!!.cancel()
            secureAcceptThread = null
        }
        if (insecureAcceptThread != null) {
            insecureAcceptThread!!.cancel()
            insecureAcceptThread = null
        }
        bluetoothState = STATE_NONE
        // Update UI title
        updateUserInterfaceTitle()
    }

    override fun write(out: ByteArray) {
        // Create temporary object
        var r: ConnectedThread?
        // Synchronize a copy of the ConnectedThread
        synchronized(this) {
            if (bluetoothState != STATE_CONNECTED) return
            r = connectedThread
        }
        // Perform the write unsynchronized
        r!!.write(out)
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private fun connectionFailed() {
        // Send a failure message back to the Activity
        btCallBackListener.onConnectionFailed()
        bluetoothState = STATE_NONE
        // Update UI title
        updateUserInterfaceTitle()

        // Start the service over to restart listening mode
        start()
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private fun connectionLost() {
        // Send a failure message back to the Activity
        btCallBackListener.onDisconnected()
        bluetoothState = STATE_NONE
        // Update UI
        updateUserInterfaceTitle()

        // Start the service over to restart listening mode
        start()
    }

    override fun getPairedDevices(): Set<BluetoothDevice> {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter.bondedDevices
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private inner class AcceptThread : Thread() {
        // The local server socket
        private val serverSocket: BluetoothServerSocket?
        override fun run() {
            name = "RealwearCompanionAcceptThread"
            var socket: BluetoothSocket? = null

            // Listen to the server socket if we're not connected
            while (bluetoothState != STATE_CONNECTED) {
                socket = try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    serverSocket!!.accept()
                } catch (e: IOException) {
                    e.printStackTrace()
                    break
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized(this@BluetoothService) {
                        when (bluetoothState) {
                            STATE_LISTEN, STATE_CONNECTING ->                                 // Situation normal. Start the connected thread.
                                connected(socket, socket.remoteDevice)
                            STATE_NONE, STATE_CONNECTED ->                                 // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close()
                                } catch (e: IOException) {
                                    e.printStackTrace()
                                }
                        }
                    }
                }
            } }

        fun cancel() {
            try {
                serverSocket!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        init {
            var bluetoothServerSocket: BluetoothServerSocket? = null
            // Create a new listening server socket
            try {
                bluetoothServerSocket =  bluetoothAdapter.listenUsingRfcommWithServiceRecord("RealwearCompanion", UUID_SECURE)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            serverSocket = bluetoothServerSocket
            bluetoothState = STATE_LISTEN
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private inner class ConnectThread(private val device: BluetoothDevice, secure: Boolean) :
        Thread() {
        private val socket: BluetoothSocket?
        override fun run() {
            name = "RealwearCompanionConnectThread"
            // Always cancel discovery because it will slow down a connection
            bluetoothAdapter.cancelDiscovery()

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                socket!!.connect()
            } catch (e: IOException) {
                // Close the socket
                try {
                    socket!!.close()
                } catch (e2: IOException) {
                    e2.printStackTrace()
                }
                connectionFailed()
                return
            }

            // Reset the ConnectThread because we're done
            synchronized(this@BluetoothService) { connectThread = null }

            // Start the connected thread
            connected(socket, device)
        }

        fun cancel() {
            try {
                socket!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        init {
            var bluetoothSocket: BluetoothSocket? = null
            try {
                bluetoothSocket =  device.createRfcommSocketToServiceRecord(UUID_SECURE)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            socket = bluetoothSocket
            bluetoothState = STATE_CONNECTING
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private inner class ConnectedThread(socket: BluetoothSocket?) : Thread() {
        private val bluetoothSocket: BluetoothSocket?
        private val inputStream: InputStream?
        private val outputStream: OutputStream?
        override fun run() {
            val buffer = ByteArray(1024)
            var bytes: Int
            // Keep listening to the InputStream while connected
            while (bluetoothState == STATE_CONNECTED) {
                try {
                    bytes = inputStream!!.read(buffer)
                    btCallBackListener.onMessageReceived(bytes, buffer)
                } catch (e: IOException) {
                    e.printStackTrace()
                    connectionLost()
                    break
                }
            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        fun write(buffer: ByteArray?) {
            try {
                outputStream!!.write(buffer)

                // Share the sent message back to the UI Activity
                btCallBackListener.onMessageSent(buffer!!)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        fun cancel() {
            try {
                bluetoothSocket!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        init {
            bluetoothSocket = socket
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket!!.inputStream
                tmpOut = socket.outputStream
            } catch (e: IOException) {
                e.printStackTrace()
            }
            inputStream = tmpIn
            outputStream = tmpOut
            bluetoothState = STATE_CONNECTED
        }
    }

    companion object {
        const val STATE_NONE = 0
        const val STATE_LISTEN = 1
        const val STATE_CONNECTING = 2
        const val STATE_CONNECTED = 3
        private val UUID_SECURE = UUID.fromString("8ff23dc5-503b-41a3-8db0-e6ed5c6ee782")
    }

    init {
        bluetoothState = STATE_NONE
        newBluetoothState = bluetoothState
        btCallBackListener = bluetoothCallBackListener
    }
}