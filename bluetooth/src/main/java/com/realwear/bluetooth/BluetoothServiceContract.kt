package com.realwear.bluetooth

import android.bluetooth.BluetoothDevice

interface BluetoothServiceContract {
    /**
     * Returns the current state of the connection
     */
    fun getBluetoothState() : Int

    /**
     * Starts bluetooth service
     */
    fun start()

    /**
     * connects to a specified bluetooth device with security on or off
     */
    fun connect(bluetoothDevice: BluetoothDevice, secure : Boolean)

    /**
     * Stops current running bluetooth connection
     */
    fun stop()

    /**
     * Writes data on the bluetooth connection
     */
    fun write(data : ByteArray)

    /**
     * Returns already paired bluetooth devices
     */
    fun getPairedDevices() : Set<BluetoothDevice>


}