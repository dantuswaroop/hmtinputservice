package com.realwear.bluetooth

import android.bluetooth.BluetoothDevice

interface BluetoothCallBackListener {

    fun onStateChanged(state : Int)

    fun onDeviceConnected(deviceName : String, deviceAddress : String)

    fun onDisconnected()

    fun onConnectionFailed()

    fun onMessageReceived(byteCount: Int, bytes: ByteArray)

    fun onMessageSent(bytes: ByteArray)

    fun onPairedDevices(pairedDevices : List<BluetoothDevice>)


}