package com.realwear.hmtinputservice

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.bluetooth.BluetoothDevice
import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener
import android.media.AudioManager
import android.os.Build
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.ExtractedTextRequest
import androidx.annotation.RequiresApi
import com.realwear.bluetooth.BluetoothCallBackListener
import com.realwear.bluetooth.BluetoothService


@TargetApi(Build.VERSION_CODES.CUPCAKE)
@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
class HMTInputMethodService : InputMethodService(), OnKeyboardActionListener,
    BluetoothCallBackListener {
    private var keyboardView: KeyboardView? = null
    private var keyboard: Keyboard? = null
    var isCaps = false
    private var btChatService: BluetoothService? = null
    override fun onPress(primaryCode: Int) {}
    override fun onRelease(primaryCode: Int) {}
    override fun onDestroy() {
        btChatService!!.stop()
        super.onDestroy()
    }

    @SuppressLint("InflateParams")
    override fun onCreateInputView(): View {
        keyboardView = layoutInflater.inflate(R.layout.keyboard, null) as KeyboardView
        keyboard = Keyboard(this, R.xml.qwerty)
        keyboardView!!.keyboard = keyboard
        keyboardView!!.setOnKeyboardActionListener(this)
        btChatService = BluetoothService(this)
        //        connectToDevice();
        btChatService!!.start()
        return keyboardView!!
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)

//        currentInputConnection.performContextMenuAction(android.R.id.selectAll)
//        val sData: CharSequence = currentInputConnection.getSelectedText(0)
//        currentInputConnection.

        val extractedText = currentInputConnection.getExtractedText(ExtractedTextRequest(), 0)
        var text = ""
        if (extractedText != null && extractedText.text != null) {
            text = extractedText.text.toString()
        }

        val stringBuilder = StringBuilder()
        stringBuilder.append("{")
        stringBuilder.append("\"inputType\"")
        stringBuilder.append(":")
        stringBuilder.append(info?.inputType)
        stringBuilder.append(",")
        stringBuilder.append("\"text\"")
        stringBuilder.append(":")
        stringBuilder.append("\"")
        stringBuilder.append(text)
        stringBuilder.append("\"")
        stringBuilder.append("}")

        println(" editor is  :: " + stringBuilder.toString())
        btChatService?.write(stringBuilder.toString().toByteArray())

    }

    override fun onShowInputRequested(flags: Int, configChange: Boolean): Boolean {
        return super.onShowInputRequested(flags, configChange)
    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray) {
        val inputConnection = currentInputConnection
        playClick(primaryCode)
        when (primaryCode) {
            Keyboard.KEYCODE_DELETE -> inputConnection.deleteSurroundingText(1, 0)
            Keyboard.KEYCODE_SHIFT -> {
                isCaps = !isCaps
                keyboard!!.isShifted = isCaps
                keyboardView!!.invalidateAllKeys()
            }
            Keyboard.KEYCODE_DONE -> inputConnection.sendKeyEvent(
                KeyEvent(
                    KeyEvent.ACTION_DOWN,
                    KeyEvent.KEYCODE_ENTER
                )
            )
            else -> {
                var code = primaryCode.toChar()
                if (Character.isLetter(code) && isCaps) {
                    code = Character.toUpperCase(code)
                }
                inputConnection.commitText(code.toString(), 1)
            }
        }
    }

    private fun playClick(i: Int) {
        val audioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        when (i) {
            32 -> audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR)
            Keyboard.KEYCODE_DONE, 10 -> audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN)
            Keyboard.KEYCODE_DELETE -> audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE)
            else -> audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD)
        }
    }

    override fun onText(text: CharSequence) {}
    override fun swipeLeft() {}
    override fun swipeRight() {}
    override fun swipeDown() {}
    override fun swipeUp() {}
    override fun onStateChanged(state: Int) {}
    override fun onDeviceConnected(deviceName: String, deviceAddress: String) {
//        Toast.makeText(this, "Bluetooth device connected ${deviceName}}", Toast.LENGTH_LONG).show()
    }
    override fun onDisconnected() {}
    override fun onConnectionFailed() {
//        Toast.makeText(this, "Bluetooth connection failed", Toast.LENGTH_LONG).show()
    }

    override fun onMessageReceived(byteCount: Int, bytes: ByteArray) {
//        val keyCode: Int = String(bytes, 0, byteCount).toInt()
        val text = String(bytes, 0, byteCount)
//        currentInputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, keyCode))
//        currentInputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, keyCode))
        val currentText: CharSequence = currentInputConnection.getExtractedText(ExtractedTextRequest(), 0).text
        val beforCursorText: CharSequence = currentInputConnection.getTextBeforeCursor(currentText.length, 0)
        val afterCursorText: CharSequence = currentInputConnection.getTextAfterCursor(currentText.length, 0)
        currentInputConnection.deleteSurroundingText(beforCursorText.length, afterCursorText.length)

        currentInputConnection.commitText(text, 0)
    }

    override fun onMessageSent(bytes: ByteArray) {}
    override fun onPairedDevices(pairedDevices: List<BluetoothDevice>) {}
}